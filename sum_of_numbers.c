#include <stdio.h>
int input_size()
{
    int n;
    printf("Enter the number of elements to be added\n");
    scanf("%d",&n);
    return n;
}

void input_array(int n, int arr[n])
{
	  for(int i=0;i<n;i++)
	  {
	    	printf("element %d:\n",i+1);
		scanf("%d",&arr[i]);
	  }
}

int add(int n, int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++) 
    {
        sum = sum+a[i];
    }
    return sum;
}

void output(int sum)
{
	 printf("The sum is %d\n",sum);
}
int main()
{
    int n,sum;
    n = input_size();
    int a[n];
    input_array(n,a);
    sum=add(n,a);
    output(sum);
    return 0;
}

