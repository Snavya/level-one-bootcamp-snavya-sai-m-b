#include <stdio.h>
float number();
float volume(float,float,float);
void print(float);
int main()
{
    printf("Enter the height, depth and width respectively\n");
    float h,d,b,v;
    h=number();
    d=number();
    b=number();
    v=volume(h,d,b);
    print(v);
    return 0;
}
float number()
{
   float a;
   printf("Enter the dimensions of tromboloid");
   scanf("%f",&a);
   return a;
}

float volume(float h,float d,float b)
{
    float volume;
    volume = 1.0/3*((h*d) + d)/b;
    return volume;
    
}

void print(float v)
{
    printf("Volume of tromboloid is %f",v);
}
//WAP to find the volume of a tromboloid using 4 functions.